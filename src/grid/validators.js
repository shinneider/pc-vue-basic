export const gridAvailableSizes = [
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "11",
  "12",
];

export const gridColumnSizes = [...gridAvailableSizes, ...["", "auto"]];

export const orderColumnPosition = [
  ...gridAvailableSizes,
  ...["first", "last"],
];

export const gridSizeValidator = (value) => {
  return gridAvailableSizes.indexOf(value) !== -1;
};

export const gridColumnSizesValidator = (value) => {
  return gridColumnSizes.indexOf(value) !== -1;
};

export const orderColumnValidator = (value) => {
  return orderColumnPosition.indexOf(value) !== -1;
};
