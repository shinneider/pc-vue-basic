import { gridSizeValidator, gridColumnSizesValidator } from "./validators.js";

export const columnsProps = (params = { type: "size" }) => {
  if (["column", "size", ""].indexOf(params.type) === -1)
    throw new Error(
      `'type' must be column or size, default is size, received ${params.type}`
    );

  let defaultProps = {
    validator: (val) =>
      params.type === "column"
        ? gridColumnSizesValidator(val)
        : gridSizeValidator(val),
  };

  return {
    colXs: {
      ...defaultProps,
    },
    colSm: {
      ...defaultProps,
    },
    colMd: {
      ...defaultProps,
    },
    colLg: {
      ...defaultProps,
    },
    colXl: {
      ...defaultProps,
    },
  };
};

const mountColumnGrid = (size, val) => {
  if (val === undefined) return "";
  return val === ""
    ? `col${size !== "xs" ? "-" + size : ""}`
    : `col${size !== "xs" ? "-" + size : ""}-${val}`;
};

export const columnsClass = () => {
  return {
    columnsClass() {
      return ` ${mountColumnGrid("xs", this.colXs)}
               ${mountColumnGrid("sm", this.colSm)}
               ${mountColumnGrid("md", this.colMd)}
               ${mountColumnGrid("lg", this.colLg)}
               ${mountColumnGrid("xl", this.colXl)}
             `;
    },
  };
};
