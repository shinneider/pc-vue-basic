import { orderColumnValidator } from "./validators.js";

export const orderProps = () => {
  return {
    order: {
      validator: (val) => orderColumnValidator(val),
    },
  };
};

export const offsetClass = () => {
  return {
    offsetClass() {
      if (val === undefined) return "";
      return `order-${this.order}`;
    },
  };
};
