import { gridSizeValidator } from "./validators.js";

export const offsetProps = () => {
  let defaultProps = {};
  defaultProps.validator = (val) => gridSizeValidator(val);

  return {
    offsetXs: {
      ...defaultProps,
    },
    offsetSm: {
      ...defaultProps,
    },
    offsetMd: {
      ...defaultProps,
    },
    offsetLg: {
      ...defaultProps,
    },
    offsetXl: {
      ...defaultProps,
    },
  };
};

const mountOffsetClass = (size, val) => {
  if (val === undefined) return "";
  return `col-${size}-${val}`;
};

export const offsetClass = () => {
  return {
    offsetClass() {
      return ` ${mountOffsetClass("xs", this.colXs)}
               ${mountOffsetClass("sm", this.colSm)}
               ${mountOffsetClass("md", this.colMd)}
               ${mountOffsetClass("lg", this.colLg)}
               ${mountOffsetClass("xl", this.colXl)}
             `;
    },
  };
};
