export const inList = (value, list) => {
  return list.indexOf(value) !== -1;
};

export const betweenNumbers = (value, min, max) => {
  return min < value && value < max;
};
