var webpack = require("webpack");
const path = require("path");
const VueLoaderPlugin = require("vue-loader/lib/plugin");

module.exports = {
  entry: {
    gridColumns: "./src/grid/columns.js",
    gridOffset: "./src/grid/offset.js",
    gridOrder: "./src/grid/order.js",
    gridValidators: "./src/grid/validators.js",
  },
  output: {
    path: path.resolve(__dirname + "/dist/"),
    publicPath: "dist/",
    filename: "[name].js",
    libraryTarget: "umd",
    library: "[name]",
    umdNamedDefine: true,
  },
  mode: "production",
  optimization: {
    minimize: true,
  },
  resolve: {
    extensions: [".js"],
    alias: {
      vue$: "vue/dist/vue.common.js",
    },
  },
  devtool: "#source-map",
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: path.resolve(__dirname, "node_modules"),
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
          },
        },
      },
      {
        test: /\.vue$/,
        use: "vue-loader",
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(woff|woff2|eot|ttf|svg)(\?.*$|$)/,
        use: {
          loader: "file-loader",
          options: {
            name: "fonts/[name].[ext]",
            esModule: false,
          },
        },
      },
    ],
  },
  externals: {
    vue: "Vue",
  },
  plugins: [
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1,
    }),
    new VueLoaderPlugin(),
  ],
};
